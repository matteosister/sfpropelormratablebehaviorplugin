--------------------------------
sfPropelORMRatableBehaviorPlugin
--------------------------------

A behaviour for propel 1.6.x and symfony 1.4.x to rate your objects

How to install
--------------

- add this plugin as a submodule

    git submodule add https://bitbucket.org/matteosister/sfpropelormratablebehaviorplugin.git plugins/sfPropelORMRatableBehaviorPlugin

- enable the plugin in your **ProjectConfiguration** class

*config/ProjectConfiguration.class.php*

::

    <?php

    require_once dirname(__FILE__) . '/../lib/vendor/symfony/autoload/sfCoreAutoload.class.php';
    sfCoreAutoload::register();

    class ProjectConfiguration extends sfProjectConfiguration
    {
      public function setup()
      {
        // ...
        $this->enablePlugins('sfPropelORMRatableBehaviorPlugin');
      }
    }

- add the **ratable** behavior to a class in your schema file

*config/schema.xml*

::

    <table name="article">
        <behavior name="ratable" />
        <!-- ... -->
    </table>

- rebuild your model

::

    php symfony propel:build-all



Columns And Method generated
----------------------------

The behavior **adds two column to the object**. The average rating, and the total number of votes.
Here is a full parameters specification with default values.

::

    <behavior name="ratable">
        <!-- the db name of the average column -->
        <parameter name="average_column" value="the_average"></parameter>
        <!-- the db name of the votes number column -->
        <parameter name="nb_votes_column" value="ratable_nb_votes"></parameter>
        <!-- maximum vote, from 1 to ... -->
        <parameter name="max_vote" value="10" />
    </behavior>


In the base class of your object model you'll find two new methods:

::

    public function getRating($max = ...) // $max default value comes from the schema parameters
    public function setRating($rating)

see the class comments for reference

How to use
----------

::
    
    <?php
    $article = new Article(); // nb_votes: 0, avg: null

    $article->setRating(6); // you need to call the save() method to actually write the data to db.
    $article->save() // nb_votes: 1, avg: 6

    $article->setRating(10);
    $article->save(); // nb_votes: 2, avg: 8

    $article->getRating(); // 6 out of 10
    $article->getRating(20); // 16 out of 20